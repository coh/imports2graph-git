from setuptools import setup
import os
import os.path
from imports2graph.__project__ import version


# since hg can't track empty directories...
try:
    os.makedirs(os.path.join('ulp','doc', 'build'))
except OSError:
    pass


# these two functions are taken from
# http://wiki.python.org/moin/Distutils/Cookbook/AutoPackageDiscovery
def is_package(path):
    return (
        os.path.isdir(path) and
        os.path.isfile(os.path.join(path, '__init__.py'))
    )


def find_packages(path, base=''):
    ''' find all packages in path '''
    packages = {}
    for item in os.listdir(path):
        dir = os.path.join(path, item)
        if is_package( dir ):
            if base:
                module_name = "%(base)s.%(item)s" % vars()
            else:
                module_name = item
            packages[module_name] = dir
            packages.update( find_packages(dir, module_name) )
    return packages


install_reqs = [
#    'blockdiag>=0.6.1',
#    'flexmock>=0.7.1',
#    'nose>=0.11.3',
    'Sphinx>=0.6.8',
#    'sphinxcontrib-blockdiag>=0.6.1',
#    'SQLAlchemy>=0.6',
]

setup(
    name='imports2graph',
    version=version(),
    description='turn the import hierarchy of one or more files/directories'
        ' into a graph.',
    author='Clemens-O. Hoppe',
    author_email='coh@co-hoppe.net',
    url='http://bitbucket.org/coh/imports2graph/',
    install_requires = install_reqs,
    packages = find_packages('.'),
    test_suite="nose.collector",
)
