from __future__ import print_function
import argparse
import bisect
import compiler
import compiler.ast
import os
import os.path
import re
import sys

from imports2graph.writers.interface import IWriter
from imports2graph.writers.dot import DotWriter
from imports2graph.writers.graphml import GraphmlWriter
from imports2graph.writers.gml import GmlWriter


NODE_COLOR = '#c0c0c0'
STD_LIB_COLOR = '#008080'


def read_stdlib(filename):
    try:
        return open(filename, 'r').read().split('\n')
    except Exception:
        return []


_std_lib = [
    'BaseHTTPServer',       'Bastion',              'CGIHTTPServer',
    'ColorPicker',          'ConfigParser',         'Cookie',
    'DocXMLRPCServer',      'EasyDialogs',          'FrameWork',
    'HTMLParser',           'MacOS',                'MimeWriter',
    'ScrolledText',         'SimpleHTTPServer',     'SimpleXMLRPCServer',
    'SocketServer',         'StringIO',             'Tix',
    'Tkinter',              'UserDict',             'UserList',
    'UserString',
    '__builtin__',          '__future__',           '__main__',
    '_winreg',
    'abc',                  'aifc',                 'anydbm',
    'argparse',
    'array',                'asynchat',             'asyncore',
    'atexit',               'audioop',              'autoGIL',
    'base64',               'bdb',                  'binascii',
    'binhex',               'bisect',               'bsddb',
    'bz2',                  'cPickle',              'cStringIO',
    'calendar',             'cgi',                  'cgitb',
    'chunk',                'cmath',                'cmd',
    'code',                 'codecs',               'codeop',
    'collections',          'colorsys',             'commands',
    'compileall',           'compiler',             'compiler.ast',
    'contextlib',           'cookielib',            'copy',
    'copy_reg',             'crypt',                'ctypes',
    'curses',               'curses.ascii',         'curses.panel',
    'curses.textpad',       'curses.wrapper',       'cvs',
    'datetime',             'dbhash',               'dbm',
    'decimal',              'difflib',              'dircache',
    'dis',                  'distutils',            'dl',
    'doctest',              'dumbdbm',              'dummy_thread',
    'dummy_threading',      'email',                'errno',
    'fcntl',                'filecmp',              'fileinput',
    'findertools',          'fnmatch',              'formatter',
    'fpectl',               'fpformat',             'fractions',
    'ftplib',               'functools',            'future_builtins',
    'gc',                   'gdbm',                 'getopt',
    'getpass',              'gettext',              'glob',
    'grp',                  'gzip',                 'hashlib',
    'heapq',                'hmac',                 'hotshot',
    'htmlentitydefs',       'htmllib',              'httplib',
    'ic',                   'imageop',              'imaplib',
    'imghdr',               'imp',                  'imputil',
    'inspect',              'io',                   'itertools',
    'json',                 'keyword',              'linecache',
    'locale',               'logging',              'macostools',
    'macpath',              'mailbox',              'mailcap',
    'marshal',              'math',                 'md5',
    'mhlib',                'mimetools',            'mimetypes',
    'mimify',               'mmap',                 'modulefinder',
    'msilib',               'msvcrt',               'multifile',
    'multiprocessing',      'mutex',                'netrc',
    'new',                  'nis',                  'nntplib',
    'numbers',              'operator',             'optparse',
    'os',                   'os.path',              'ossaudiodev',
    'parser',               'pdb',                  'pickle',
    'pickletools',          'pipes',                'pkgutil',
    'platform',             'plistlib',             'popen2',
    'poplib',               'posix',                'posixfile',
    'pprint',               'pty',                  'pwd',
    'py_compile',           'pyclbr',               'pydoc',
    'queue',                'quopri',               'random',
    're',                   'readline',             'repr',
    'resource',             'rexec',                'rfc822',
    'rlcompleter',          'robotparser',          'runpy',
    'sched',                'select',               'sets',
    'sgmllib',              'sha',                  'shelve',
    'shlex',                'shutil',               'signal',
    'site',                 'smtpd',                'sndhdr',
    'socket',               'spwd',                 'sqlite3',
    'ssl',                  'stat',                 'statvfs',
    'string',               'struct',               'subprocess',
    'sunau',                'symbol',               'symtable',
    'sys',                  'syslog',               'tabnanny',
    'tarfile',              'telnetlib',            'tempfile',
    'termios',              'test',                 'test.test_support',
    'textwrap',             'thread',               'threading',
    'time',                 'timeit',               'token',
    'tokenize',             'trace',                'traceback',
    'tty',                  'turtle',               'types',
    'unicodedata',          'unittest',             'urllib',
    'urllib2',              'urlparse',             'user',
    'uu',                   'uuid',                 'warnings',
    'wave',                 'weakref',              'webbrowser',
    'whichdb',              'winsound',             'wsgiref',
    'xdrlib',               'xml.dom',              'xml.dom.minidom',
    'xml.dom.pulldom',      'xml.etree.ElementTree', 'xml.parsers.expat',
    'xml.sax',              'xml.sax.handler',      'xml.sax.saxutils',
    'xml.sax.xmlreader',    'xmlrpclib',            'zipfile',
    'zipimport',            'zlib'
]




"""
each node is a string which is its name.
each edge is a pair of (X, Y) with the indication that module X imports module
Y.
"""
_graph_data = {
    'nodes': [],
    'edges': [],
}


def shorten_progress(filename, max_len=65):
    if len(filename) < max_len:
        return filename
    else:
        path_portions = filename.split(os.sep)
        pdict = {}
        total_len = 0
        front_parts = []
        back_parts = []
        for part in path_portions:
            if part:
                pdict[part] = len(part)+1
        for part in path_portions:
            if part in pdict.keys():
                total_len += pdict[part]
                front_parts.append(part)
                break
        for part in reversed(path_portions):
            if part in pdict.keys() and total_len + pdict[part] < max_len-5:
                back_parts = [part] + back_parts
                total_len += pdict[part]
            elif part in pdict.keys():
                break

        return os.sep.join(front_parts + ['...'] + back_parts)


def sanitize_filename(filename):
    return re.sub('.py$', '', filename).replace(os.sep, '.').lstrip('.')


def _add_item(module, imports, strip):
    for prefix in strip:
        if prefix and module.startswith(prefix):
            module = module.replace(prefix, '')
    if module not in _graph_data['nodes']:
        bisect.insort(_graph_data['nodes'], module)
    if imports not in _graph_data['nodes']:
        bisect.insort(_graph_data['nodes'], imports)
    _graph_data['edges'].append((module, imports))


def add_to_graph(the_module, filename, strip):
    pyfilename = sanitize_filename(filename)
    for node in the_module.node.nodes:
        if isinstance(node, compiler.ast.From):
            if not isinstance(node.modname, basestring):
                for item in node.modname:
                    _add_item(pyfilename, item, strip)
            else:
                _add_item(pyfilename, node.modname, strip)
        elif isinstance(node, compiler.ast.Import):
            for mod in node.names:
                if mod:
                    _add_item(pyfilename, mod[0], strip)


def parse_directory(dirname, strip):
    for root, dirs, files in os.walk(dirname):
        for file_ in files:
            if file_.endswith('.py'):
                parse_file(os.path.normpath(os.path.join(root, file_)), strip)


def parse_file(filename, strip):
    sf = shorten_progress(filename)
    print('\r', ' '*79, '\rparsing: %s' % (sf), end='', file=sys.stderr)
    try:
        mod = compiler.parseFile(filename)
        add_to_graph(mod, filename, strip)
    except SyntaxError as e:
        print('\r', ' '*79, 'failed to parse %s:\n%s' % (filename, e),
            file=sys.stderr)


def write_graph(writers):
    if not isinstance(writers, (list, tuple)):
        writers = [writers,]
    for writer in writers:
        writer.write(_graph_data['nodes'], _graph_data['edges'])


def create_arg_parser():
    cl_args = argparse.ArgumentParser(description = 
        'convert an import hierarchy to a graph')
    cl_args.add_argument('files_or_dirs', metavar='fil_or_dir', type=unicode,
        nargs='*', help='a number of files or directories containing python '
        'sources', default=[])
    cl_args.add_argument('-f', '--format', action='store', type=unicode,
        default='dot')
    cl_args.add_argument('--list-writers', action='store_true',
        help='list all writers and exit')
    cl_args.add_argument('--strip', action='append', default=[],
        help='one or more prefixes to strip from the paths')
    return cl_args


def list_writers(output):
    for sc in IWriter.__subclasses__():
        tmp = sc.format_name()
        if tmp:
            print('%s\t%s' %(tmp, sc.__name__), file=output)


def main():
    cl_args = create_arg_parser()
    args = cl_args.parse_args()

    strip = []
    for a in args.strip:
        strip.append(('.'.join(a.split(os.sep)) + '.').lstrip('.'))

    if args.list_writers:
        list_writers(sys.stdout)
        return 0

    if len(sys.argv) <= 1:
        print("Syntax: %s [file_or_dir1 [file_or_dir2 [...]]]" % sys.argv[0],
            file=sys.stderr)
        print("assuming current directory as base", file=sys.stderr)
        sys.argv[1:] = ['.']

    for item in sys.argv[1:]:
        if os.path.isdir(item):
            parse_directory(item, strip)
        elif item.endswith('.py'):
            parse_file(item, strip)
    print('\n', file=sys.stderr)
    write_graph( GmlWriter(stdlib=_std_lib) )

if __name__ == '__main__':
    sys.exit(main())


