from __future__ import print_function
import sys

from imports2graph.writers.interface import IWriter


class DotWriter(IWriter):
    def __init__(self, stdlib=[], nodecolor='#c0c0c0', stdlibcolor='#008080'):
        self._nodecolor = nodecolor
        self._stdlibcolor = stdlibcolor
        self._stdlib = stdlib

    @staticmethod
    def format_name():
        return 'dot'

    def write_header(self, output):
        print('''digraph G {
\tnode [fontname=Verdana, fontsize=7, style=filled, shape=rect, color="%s"];
''' % self._nodecolor, file=output)

    def write_footer(self, output):
        print('\n}', file=output)

    def write(self, nodelist, edgelist, output=sys.stdout):
        self.write_header(output)
        node_ids = dict([(x[1], x[0]) for x in enumerate(nodelist)])
        for node in nodelist:
            if node in self._stdlib:
                print('\tn%s [label="%s", color="%s"];' % (
                    node_ids[node], node, self._stdlibcolor), file=output)
            else:
                print('\tn%s [label="%s"];' % (node_ids[node], node),
                    file=output)
        for from_, to_ in edgelist:
            print('\tn%s -> n%s;' % (node_ids[from_], node_ids[to_]),
                file=output)
        self.write_footer(output)
