''' Defines the interface for a graph writer
'''
import abc
import sys
import platform


abstractstaticmethod = None

if platform.python_version_tuple() >= ('3', '2', '0'):
    print platform.python_version_tuple()
    abstractstaticmethod = abc.abstractstaticmethod
else:
    class AbstractStaticMethodImpl(staticmethod):
        __isabstractmethod__ = True

        def __init__(self, callable):
            callable.__isabstractmethod__ = True
            super(AbstractStaticMethodImpl, self).__init__(callable)

    abstractstaticmethod = AbstractStaticMethodImpl


class IWriter(object):
    ''' this class has to be inherited from a graph writer
    '''
    __metaclass__ = abc.ABCMeta

    @abstractstaticmethod
    def format_name():
        ''' return the format name for the writer
        '''
        return None

    @abc.abstractmethod
    def write(self, nodelist, edgelist, output=sys.stdout):
        ''' write the graph consisting of nodelist and edgelist to output.

            nodelist is a normal list containing the node names,
            edgelist is a list containing pairs (2-tuples) where the first
            entry if the from- and the second entry the to-node.

            output must be a file-like object opened for writing.
        '''
        pass
