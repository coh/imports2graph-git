from __future__ import print_function
import sys

from imports2graph.writers.interface import IWriter

class GraphmlWriter(IWriter):
    def __init__(self, stdlib=[], nodecolor='#c0c0c0', stdlibcolor='#008080'):
        self._nodecolor = nodecolor
        self._stdlibcolor = stdlibcolor
        self._stdlib = stdlib

    @staticmethod
    def format_name():
        return 'graphml'

    def write_header(self, output):
        print('''<?xml version="1.0" encoding="UTF-8"?>
<graphml xmlns="http://graphml.graphdrawing.org/xmlns"  
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://graphml.graphdrawing.org/xmlns
     http://graphml.graphdrawing.org/xmlns/1.0/graphml.xsd">
<key id="d0" for="node" attr.name="color" attr.type="string">
    <default>yellow</default>
  </key>
  <graph id="G" edgedefault="directed">''', file=output)

    def write_footer(self, output):
        print('''
  </graph>
</graphml>''', file=output)

    def write(self, nodelist, edgelist, output=sys.stdout):
        self.write_header(output)
        node_ids = dict([(x[1], x[0]) for x in enumerate(nodelist)])

        for node in nodelist:
            thecol = self._stdlibcolor \
                    if node in self._stdlib else self._nodecolor
            print('''    <node id="n%s">
        <data key="d0">%s</data>
    </node>''' % (node_ids[node], thecol), file=output)

        edge_counter = 0
        for from_, to_ in edgelist:
            print('''    <edge id="e%s" source="n%s" target="n%s" />''' % 
                (edge_counter, node_ids[from_], node_ids[to_]), file=output)
            edge_counter += 1

        self.write_footer(output)
