from __future__ import print_function
import sys

from imports2graph.writers.interface import IWriter


class GmlWriter(IWriter):
    def __init__(self, stdlib=[], nodecolor='#c0c0c0', stdlibcolor='#008080'):
        self._nodecolor = nodecolor
        self._stdlibcolor = stdlibcolor
        self._stdlib = stdlib

    @staticmethod
    def format_name():
        return 'gml'

    def write_header(self, output):
        print('''graph [
    version 0
    directed 1''', file=output)

    def write_footer(self, output):
        print('\n]', file=output)

    def write(self, nodelist, edgelist, output=sys.stdout):
        self.write_header(output)
        node_ids = dict([(x[1], x[0]) for x in enumerate(nodelist)])
        for node in nodelist:
            thecol = self._stdlibcolor \
                    if node in self._stdlib else self._nodecolor
            print('''    node [
        id %s
        label "%s"
        graphics [
            w 192.0
            h 32.0
            fill "%s"
        ]
    ]''' % (node_ids[node], node, thecol), file=output)
        for from_, to_ in edgelist:
            print('''    edge [
        source %s
        target %s
    ]''' % (node_ids[from_], node_ids[to_]), file=output)
        self.write_footer(output)
